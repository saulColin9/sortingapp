package org.sorting;

import org.junit.Test;


import static org.junit.Assert.assertArrayEquals;

public class AppTest {

    @Test(expected = IllegalArgumentException.class)
    public void invalidInput_Should_Throw_Exception() {
        Sorter sorter = new Sorter();
        sorter.sort(null);
    }

    @Test
    public void validInput_Should_Sort_Array() {
        Sorter sorter = new Sorter();
        String[] args = new String[]{
                "44",
                "5",
                "9",
                "100",
                "12",
        };
        int[] array = sorter.sort(args);
        assertArrayEquals(new int[]{
                5, 9, 12, 44, 100
        }, array);
    }

    @Test
    public void singleInput_Should_Return_Single_Item_Array() {
        Sorter sorter = new Sorter();
        String[] args = new String[]{"100"};
        int[] array = sorter.sort(args);
        assertArrayEquals(new int[]{100}, array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void over_Ten_Input_Values_Should_Throw_Exception() {
        Sorter sorter = new Sorter();
        String[] args = new String[]{
                "44",
                "5",
                "9",
                "100",
                "12",
                "44",
                "5",
                "9",
                "100",
                "12",
                "9",
        };
        sorter.sort(args);
    }

    @Test
    public void negative_Input_Should_Sort_Array() {
        Sorter sorter = new Sorter();
        String[] args = new String[]{
                "-44",
                "-5",
                "-9",
                "-100",
                "12",
        };
        int[] array = sorter.sort(args);
        assertArrayEquals(new int[]{
                -100, -44, -9, -5, 12
        }, array);

    }

    @Test
    public void ten_Input_Values_Should_Sort_Array() {
        Sorter sorter = new Sorter();
        String[] args = new String[]{

                "44", "5", "9", "100", "12",
                "44", "5", "9", "100", "12"
        };

        int[] array = sorter.sort(args);
        assertArrayEquals(
                new int[]{
                        5, 5, 9, 9, 12, 12, 44, 44, 100, 100
                },
                array
        );
    }
}
