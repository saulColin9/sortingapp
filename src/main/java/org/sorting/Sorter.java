package org.sorting;

import java.util.Arrays;

public class Sorter {

    public int[] sort(String[] args) {
        if (args == null || args.length > 10) {
            throw new IllegalArgumentException();
        }
        int[] array = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            array[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(array);
        for (int num : array) {
            System.out.println(num);
        }
        return array;
    }
}
